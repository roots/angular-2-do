## Angular 2 workshop files ##

steps to get started
--------------------
 
 - go to http://codeanywhere.com
 - sign into your account (or create your free account)
 - connect your first container 
    - choose one of the Angular containers
    - wait for the container to start
    

Once your container has started
-------------------------------

- run these commands

    - `rm -rf ../workspace/*`
    - `rm .HIDDEN FILENAME`
    
    
    
Once your container webroot is empty
------------------------------------

- run this command
    - `git clone http://gitlab.oit.duke.edu/roots/angular-2-do.git .`
    - `git checkout 1.0`
    - `npm install`
    - `npm start`

Then...
-------

visit http://port-3000.angular-YOURUSERNAME.codeanyapp.com



Some Helpful Links
------------------

https://angular.io/docs/ts/latest/tutorial/
Borrowed heavily from this excellent tutorial

https://github.com/tastejs/todomvc/tree/gh-pages/examples/angular2
Hey, a To-Do list in angular 2! Sounds familiar! Pretty well written.

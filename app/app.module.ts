import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

// Our App Components and Services
import { AppComponent }  from './app.component';
import { TaskDetailComponent } from './task-detail.component';
import { TasksComponent } from './tasks.component';
import { TaskService } from './task.service';
import { CompletedComponent } from './completed.component';
import { AppRoutingModule } from './app-routing.module';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

//Contributed Components/Filters
import { OrderBy } from './orderBy.pipe'

// Main app Module
@NgModule({
  imports: [
      BrowserModule,
      FormsModule,
      AppRoutingModule,
      HttpModule,
      InMemoryWebApiModule.forRoot(InMemoryDataService)
  ],
  declarations: [
      AppComponent,
      TaskDetailComponent,
      TasksComponent,
      CompletedComponent,
      OrderBy
	],
  providers: [ TaskService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
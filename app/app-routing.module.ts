import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompletedComponent }   from './completed.component';
import { TasksComponent }      from './tasks.component';
import { TaskDetailComponent }  from './task-detail.component';

const routes: Routes = [
    {
        path: 'tasks',
        component: TasksComponent
    },
    {
        path: 'completed',
        component: CompletedComponent
    },
    {
        path: '',
        redirectTo: '/tasks',
        pathMatch: 'full'
    },
    {
        path: 'detail/:id',
        component: TaskDetailComponent
    },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}

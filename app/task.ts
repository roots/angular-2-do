export class Task {
  id: number;
  title: string;
  description: string;
  weight: number;
  complete: boolean;
}

import { Component, OnInit } from "@angular/core";

import { Router } from '@angular/router';

import { Task } from './task';

import { TaskService } from './task.service';

// Decorator
@Component({
  moduleId: module.id,
  selector: "my-tasks",
  templateUrl: 'tasks.component.html',
})

export class TasksComponent implements OnInit {
  title = "Angular 2 Do";
  tasks: Task[];
  selectedTask: Task;
    
  constructor(private taskService: TaskService, private router: Router) { }
  
  getTasks(): void {
    this.taskService
        .getTasks()
        .then(tasks => this.tasks = tasks.filter(task => task.complete === false));
  }
  
  ngOnInit(): void {
    this.getTasks();
  }

  onSelect(task: Task): void {
    this.selectedTask = task;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedTask.id]);
  }

  add(title: string): void {
    title = title.trim();
    if (!title) { return; }
    this.taskService.create(title)
        .then(task => {
          this.tasks.push(task);
          this.selectedTask = null;
        });
  }

  delete(task: Task): void {
    this.taskService
        .delete(task.id)
        .then(() => {
          this.tasks = this.tasks.filter(h => h !== task);
          if (this.selectedTask === task) { this.selectedTask = null; }
        });
  }
}


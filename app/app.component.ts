import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
    <h1>{{title}}</h1>
    <nav>
        <a routerLink="/tasks">Tasks</a>
        <a routerLink="/completed">Completed</a>
    </nav>
    
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
    title = 'My Tasks';
}
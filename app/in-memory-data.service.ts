import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        let tasks = [
            { id: 1, weight: 0, title: 'task 1', description: 'Duis autem', complete: false },
            { id: 2, weight: 10, title: 'task 2', description: 'vel eum iriure', complete: false },
            { id: 3, weight: 20, title: 'task 3', description: 'dolor in hendrerit', complete: false },
            { id: 4, weight: 30, title: 'task 4', description: 'in vulputate velit', complete: false },
            { id: 5, weight: 40, title: 'task 5', description: 'esse molestie consequat', complete: false },
            { id: 6, weight: 50, title: 'task 6', description: 'vel illum', complete: false },
            { id: 7, weight: 60, title: 'task 7', description: 'Dynama', complete: false },
            { id: 8, weight: 70, title: 'task 8', description: 'esse molestie consequat', complete: false },
            { id: 9, weight: 90, title: 'task 9', description: 'dolore eu feugiat', complete: false },
            { id: 10, weight: 80, title: 'task 10', description: 'nulla facilisis', complete: false }
        ];
        return {tasks};
    }
}
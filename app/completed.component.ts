import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { Task } from './task';
import { TaskService } from './task.service';

@Component({
    moduleId: module.id,
    selector: 'completed-tasks',
    templateUrl: 'completed.component.html'
})
export class CompletedComponent implements OnInit {

    tasks: Task[] = [];

    constructor(
        private router: Router,
        private taskService: TaskService
    ) {}

    ngOnInit(): void {
        this.taskService.getTasks()
            .then(tasks => this.tasks = tasks.filter(task => task.complete))
    }

    goToDetail(task: Task): void {
        let link = ['/detail', task.id];
        this.router.navigate(link);
    }

}